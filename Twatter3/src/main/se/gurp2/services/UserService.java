package se.gurp2.services;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import se.gurp2.models.FollowModel;
import se.gurp2.models.UserModel;
import se.gurp2.objects.JsonResponse;
import se.gurp2.objects.SearchResult;
import se.gurp2.objects.User;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Controller
public class UserService {
    private ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");

    @RequestMapping(value = "/api/login.json", method = RequestMethod.POST)
     @ResponseBody
     public LoginStatus login(@RequestParam("username") String username, @RequestParam("password") String password) {
        UserModel userModel = (UserModel) context.getBean("user");
        try {
            int id = userModel.verifyCredentials(username, password);
            UUID token = userModel.createSession(id);
            return new LoginStatus(true, token.toString());
        }catch(Exception e) {
            return new LoginStatus(false, "");
        }
    }
    @RequestMapping(value = "/api/logout.json", method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse logout(@RequestParam("token") String token) {
        UserModel userModel = (UserModel) context.getBean("user");
        try {
            userModel.logout(token);
            return new JsonResponse(true,"success");
        }catch(Exception e) {
            return new JsonResponse(false,"failure");
        }
    }
    @RequestMapping(value = "/api/verifyToken.json", method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse verifyToken(@RequestParam("token") String token) {
        UserModel userModel = (UserModel) context.getBean("user");
        int id = userModel.readToken(token);
        if(id > 0) {
            return new JsonResponse(true, "success");
        }
        return new JsonResponse(false, "failure");
    }
    @RequestMapping(value = "/api/user/infoByToken.json", method = RequestMethod.GET)
    @ResponseBody
    public User infoById(@RequestParam("token") String token) {
        UserModel userModel = (UserModel) context.getBean("user");
        return userModel.getUserByToken(token);
    }
    @RequestMapping(value = "/api/user/infoById.json", method = RequestMethod.GET)
    @ResponseBody
    public User infoById(@RequestParam("id") int id) {
        UserModel userModel = (UserModel) context.getBean("user");
        return userModel.getUser(id);
    }

    @RequestMapping(value = "/api/register.json", method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse register(@RequestParam("firstname") String firstname, @RequestParam("lastname") String lastname, @RequestParam("username") String username, @RequestParam("password") String password, @RequestParam("email") String email) {
        UserModel userModel = (UserModel) context.getBean("user");
        if (userModel.userExists(username, email)){
            return new JsonResponse(false, "exists");
        }
        userModel.create(firstname, lastname, username, password, email);
        return new JsonResponse(true, "created");
    }
    public class LoginStatus {

        private final boolean loggedIn;
        private final String token;

        public LoginStatus(boolean loggedIn, String token) {
            this.loggedIn = loggedIn;
            this.token = token;
        }

        public boolean isLoggedIn() {
            return loggedIn;
        }

        public String getToken() {
            return token;
        }
    }

    @RequestMapping(value = "/api/follow.json", method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse follow(@RequestParam("token") String token, @RequestParam("target") int target) {
        UserModel userModel = (UserModel) context.getBean("user");
        FollowModel followModel = (FollowModel) context.getBean("follow");
        try {
            int followerid = userModel.readToken(token);
            return followModel.follow(followerid,target);
        }catch(Exception e) {
            return new JsonResponse(false, "");
        }
    }
    @RequestMapping(value = "/api/getFollowers.json", method = RequestMethod.GET)
    @ResponseBody
    public List<SearchResult> getFollowers(@RequestParam("id") int id) {
        UserModel userModel = (UserModel) context.getBean("user");
        FollowModel followModel = (FollowModel) context.getBean("follow");
        List<SearchResult> results = new ArrayList<>();

        ArrayList<Integer> followers = (ArrayList<Integer>) followModel.getFollowers(id);
        for(int f: followers){
            results.add(new SearchResult(userModel.getUser(f)));
        }
        return results;

    }@RequestMapping(value = "/api/getFollowing.json", method = RequestMethod.GET)
    @ResponseBody
    public List<SearchResult> getFollowing(@RequestParam("id") int id) {
        UserModel userModel = (UserModel) context.getBean("user");
        FollowModel followModel = (FollowModel) context.getBean("follow");
        List<SearchResult> results = new ArrayList<>();

        ArrayList<Integer> following = (ArrayList<Integer>) followModel.getFollowing(id);
        for(int f: following){
            results.add(new SearchResult(userModel.getUser(f)));
        }
        return results;

    }


}
