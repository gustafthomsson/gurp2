package se.gurp2.objects;

/**
 * Created by raz on 2015-05-27.
 */
public class SearchResult {
    public String type;
    public User user;
    public Twat twat;

    public SearchResult(User user) {
        this.type = "user";
        this.user = user;
    }

    public SearchResult(Twat twat) {
        this.type = "twat";
        this.twat = twat;
    }
}
