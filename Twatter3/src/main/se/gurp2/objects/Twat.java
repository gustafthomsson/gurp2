package se.gurp2.objects;

public class Twat {
    public String timestamp;
    public long id;
    public long userid;
    public User user;
    public String text;
    public boolean retwat = false;
    public User originalPoster = new User();
    public Twat ogTwat = null;
    public Twat(long id, long userid, String text, User user) {
        this.id = id;
        this.userid = userid;
        this.text = text;
        this.user = user;
    }
    public Twat() {

    }
    public void setTimeStamp(String timestamp){
        this.timestamp = timestamp;
    }
    public long getId() {
        return id;
    }

    public long getUserid() {
        return userid;
    }

    public String getText() {
        return text;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setUserid(long userid) {
        this.userid = userid;
    }

    public void setText(String text) {
        this.text = text;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
