package se.gurp2.objects;

/**
 * Created by raz on 5/19/15.
 */
public class JsonResponse {
    public final boolean status;
    public final String message;

    public JsonResponse(boolean status, String message) {
        this.status = status;
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
