package se.gurp2.mappers;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.RowMapper;
import se.gurp2.models.TwatModel;
import se.gurp2.models.UserModel;
import se.gurp2.objects.Twat;
import se.gurp2.objects.User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
public class TwatMapper implements RowMapper<Twat> {
    public Twat mapRow(ResultSet rs, int rowNum) throws SQLException {
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        UserModel userModel = (UserModel) context.getBean("user");
        TwatModel twatModel = (TwatModel) context.getBean("twat");
        Twat twat = new Twat();
        twat.setId(rs.getInt("id"));
        twat.setUserid(rs.getInt("uid"));
        twat.setText(rs.getString("content"));
        twat.setTimeStamp(rs.getString("timestamp"));
        twat.setUser(userModel.getUser(rs.getInt("uid")));
        Pattern p = Pattern.compile("\\{retwat@(\\d+)\\}");
        Matcher m = p.matcher(twat.getText());
        if(m.find()) {
            Twat reTwat = twat;
            User reTwatter = twat.getUser();
            twat = twatModel.getTwat(Integer.valueOf(m.group(1)));
            twat.retwat = true;
            twat.originalPoster = reTwatter;
            twat.ogTwat = reTwat;

        }
        return twat;
    }

}
