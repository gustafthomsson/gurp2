package se.gurp2.interfaces;

import se.gurp2.objects.User;

import javax.sql.DataSource;

public interface UserInterface {

    void setDataSource(DataSource ds);

    void create(String firstname, String lastname, String username, String password, String email);

    int verifyCredentials(String username, String password);

    boolean logout(String token);

    User getUser(Integer id);

    boolean delete(Integer id);

}
