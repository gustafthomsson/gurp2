package se.gurp2.interfaces;

import se.gurp2.objects.Twat;

import javax.sql.DataSource;
import java.util.List;

public interface TwatInterface {

    void setDataSource(DataSource ds);

    void create(String text, Integer userid);

    Twat getTwat(Integer id);

    List<Twat> listTwats();

    List<Twat> listTwats(Integer userid);

    void delete(Integer id, Integer userid);

}
