package se.gurp2.controllers;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import se.gurp2.models.NotificationsModel;
import se.gurp2.models.UserModel;
import se.gurp2.objects.JsonResponse;
import se.gurp2.objects.SearchResult;

import java.util.List;

@Controller
public class Notifications {
    ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");

    @RequestMapping(value = "api/notifications/getall.json", method = RequestMethod.GET)
    @ResponseBody
    public List<SearchResult> getNotifications(@RequestParam("token") String token) {
        NotificationsModel notificationsModel = (NotificationsModel) context.getBean("notifications");
        UserModel userModel = (UserModel) context.getBean("user");
        int userid = userModel.readToken(token);
        return notificationsModel.getNotifications(userid);
    }
    @RequestMapping(value = "api/notifications/new.json", method = RequestMethod.GET)
    @ResponseBody
    public JsonResponse countNotifications(@RequestParam("token") String token) {
        NotificationsModel notificationsModel = (NotificationsModel) context.getBean("notifications");
        UserModel userModel = (UserModel) context.getBean("user");
        int userid = userModel.readToken(token);
        int amount = notificationsModel.getUnreadNotifications(userid);
        if(amount > 0) {
            return new JsonResponse(true, ""+amount);
        }
        return new JsonResponse(false, "0");
    }
    @RequestMapping(value = "api/notifications/markasread.json", method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse markNotificationsAsRead(@RequestParam("token") String token) {
        NotificationsModel notificationsModel = (NotificationsModel) context.getBean("notifications");
        UserModel userModel = (UserModel) context.getBean("user");
        int userid = userModel.readToken(token);
        try {
            notificationsModel.markAllAsRead(userid);
            return new JsonResponse(true, "success");
        }catch(Exception e) {
            return new JsonResponse(false, "failure");
        }
    }
}
