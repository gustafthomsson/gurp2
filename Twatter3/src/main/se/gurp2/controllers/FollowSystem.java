package se.gurp2.controllers;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import se.gurp2.models.*;
import se.gurp2.objects.JsonResponse;
import se.gurp2.objects.User;

@Controller
public class FollowSystem {
    final ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");

    @RequestMapping(value = "api/follow/followUnfollow.json", method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse followUnfollow(@RequestParam("token") String token, @RequestParam("target") int targetid) {
        NotificationsModel notificationsModel = (NotificationsModel) context.getBean("notifications");
        UserModel userModel = (UserModel) context.getBean("user");
        FollowModel followModel = (FollowModel) context.getBean("follow");
        User target;
        try {
            target = userModel.getUser(targetid);
        } catch(Exception e) {
            return new JsonResponse(false, "");
        }
        int userid = userModel.readToken(token);
        return followModel.follow(userid, (int) target.getId());
    }

    @RequestMapping(value = "api/follow/isFollowing.json", method = RequestMethod.GET)
    @ResponseBody
    public JsonResponse isFollowing(@RequestParam("token") String token, @RequestParam("target") int targetid) {
        UserModel userModel = (UserModel) context.getBean("user");
        FollowModel followModel = (FollowModel) context.getBean("follow");
        User target;
        try {
           target = userModel.getUser(targetid);
        } catch(Exception e) {
            return new JsonResponse(false, "");
        }
        int userid = userModel.readToken(token);
        if(followModel.userHasFollowed(userid, (int) target.getId())) {
            return new JsonResponse(true, "following");
        }else {
            return new JsonResponse(false, "notfollowing");
        }
    }

}
