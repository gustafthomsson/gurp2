package se.gurp2.controllers;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import se.gurp2.models.StatsModel;
import se.gurp2.models.UserModel;
import se.gurp2.objects.User;

import java.util.HashMap;

/**
 * Created by Gustaf on 15-06-01.
 */
@Controller
public class Stats {
    ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");

    @RequestMapping(value = "api/getUserStats.json", method = RequestMethod.GET)
    @ResponseBody
    public HashMap<String, Integer> getUserStats(@RequestParam("userid") Integer userid){
        StatsModel statsModel = (StatsModel) context.getBean("stats");
        UserModel userModel = (UserModel) context.getBean("user");
        HashMap<String, Integer>  map = new HashMap<>();
        try{
            User user = userModel.getUser(userid);
            map.put("following", statsModel.countFollowing(userid));
            map.put("followers", statsModel.countFollowers(userid));
            map.put("twats", statsModel.countUserTwats(userid));
        }catch (Exception e){
            e.printStackTrace();
        }
        return map;
    }
}
