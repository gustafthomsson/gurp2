package se.gurp2.controllers;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import se.gurp2.models.LoveModel;
import se.gurp2.models.TwatModel;
import se.gurp2.models.UserModel;
import se.gurp2.objects.JsonResponse;
import se.gurp2.objects.Twat;

import java.util.List;

@Controller
public class Twats {
    final ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");

    @RequestMapping(value = "api/twats/getTwat.json", method = RequestMethod.GET)
    @ResponseBody
    public Twat getTwat(@RequestParam("id") Integer id) {
        TwatModel twatModel = (TwatModel) context.getBean("twat");
        return twatModel.getTwat(id);
    }

    @RequestMapping(value = "api/twats/getAll.json", method = RequestMethod.GET)
    @ResponseBody
    public List<Twat> getTwats() {
        TwatModel twatModel = (TwatModel) context.getBean("twat");
        return twatModel.listTwats();
    }
    @RequestMapping(value = "api/twats/getUserFeed.json", method = RequestMethod.GET)
    @ResponseBody
    public List<Twat> getUserFeed(@RequestParam("token") String token) {
        TwatModel twatModel = (TwatModel) context.getBean("twat");
        UserModel userModel = (UserModel) context.getBean("user");
        int userid = userModel.readToken(token);
        return twatModel.getUsersFeed(userid);
    }

    @RequestMapping(value = "api/twats/getUsersTwats.json", method = RequestMethod.GET)
    @ResponseBody
    public List<Twat> getUserTwats(@RequestParam("userid") Integer userid) {
        TwatModel twatModel = (TwatModel) context.getBean("twat");
        return twatModel.listTwats(userid);
    }

    @RequestMapping(value = "api/twats/post.json", method= RequestMethod.POST)
    @ResponseBody
    public JsonResponse postTwat(@RequestParam("token") String token, @RequestParam("content") String content) {
        TwatModel twatModel = (TwatModel) context.getBean("twat");
        UserModel userModel = (UserModel) context.getBean("user");
        if(content.length() > 141) {
            return new JsonResponse(false, "En twat får vara max 141 tecken lång.");
        }
        int userid = userModel.readToken(token);
        if(userid != 0) {
            twatModel.create(content, userid);
            return new JsonResponse(true, "success");
        }
        return new JsonResponse(false, "failure");
    }
    @RequestMapping(value = "api/twats/deleteTwat.json", method= RequestMethod.POST)
    @ResponseBody
    public boolean deleteTwat(@RequestParam("token") String token,@RequestParam("id") Integer id) {
        TwatModel twatModel = (TwatModel) context.getBean("twat");
        UserModel userModel = (UserModel) context.getBean("user");
        int userid = userModel.readToken(token);
        if(userid != 0) {
            twatModel.delete(id,userid);
            return true;
        }
        return false;
    }
    @RequestMapping(value = "api/twats/love.json", method= RequestMethod.POST)
    @ResponseBody
    public JsonResponse loveTwat(@RequestParam("token") String token,@RequestParam("id") Integer id) {
        LoveModel loveModel = (LoveModel) context.getBean("love");
        UserModel userModel = (UserModel) context.getBean("user");
        int userid = userModel.readToken(token);
        if(userid != 0) {
            return loveModel.love(userid,id);
        }
        return new JsonResponse(false, "failed");
    }


}
