package se.gurp2.controllers;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import se.gurp2.models.SearchModel;
import se.gurp2.objects.SearchResult;

import java.util.List;

@Controller
public class Search {
    final ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");

    @RequestMapping(value = "api/search.json", method = RequestMethod.POST)
    @ResponseBody
    public List<SearchResult> search(@RequestParam("query") String query) {
        SearchModel searchModel = (SearchModel) context.getBean("search");
        return searchModel.search(query);

    }

}
