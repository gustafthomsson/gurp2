package se.gurp2.models;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import se.gurp2.interfaces.TwatInterface;
import se.gurp2.mappers.TwatMapper;
import se.gurp2.objects.Twat;
import se.gurp2.objects.User;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TwatModel implements TwatInterface {
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplate;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public void create(final String text, final Integer userid) {
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        NotificationsModel notificationsModel = (NotificationsModel) context.getBean("notifications");
        KeyHolder holder = new GeneratedKeyHolder();
        jdbcTemplate.update(new PreparedStatementCreator() {

            @Override
            public  PreparedStatement createPreparedStatement(Connection connection)
                    throws SQLException {
                PreparedStatement ps = connection.prepareStatement("INSERT INTO twats (uid, content) VALUES (?, ?)", java.sql.Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, String.valueOf(userid));
                ps.setString(2, text);
                return ps;
            }

        }, holder);

        int twatid = holder.getKey().intValue();
        Pattern pattern = Pattern.compile("(?:@([\\w\\d]+))+");
        Matcher matcher = pattern.matcher(text);
        while(matcher.find()) {
            notificationsModel.create(matcher.group(1), twatid);
        }
    }
    public List<Twat> getUsersFeed(Integer userid) {
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        UserModel userModel = (UserModel) context.getBean("user");
        User user = userModel.getUser(userid);
        FollowModel followModel = (FollowModel) context.getBean("follow");
        List<Integer> followed = followModel.getFollowing(userid);
        StringBuilder sb = new StringBuilder();
        sb.append("(");
        for(int id : followed) {
            sb.append(id).append(",");
        }
        sb.append(userid).append(")");
        String query = "SELECT * FROM twats WHERE content LIKE ? OR uid IN " + sb.toString() + " ORDER BY id DESC";
        return jdbcTemplate.query(query, new Object[]{"%@" + user.getUsername() + "%"}, new TwatMapper());
    }
    @Override
    public Twat getTwat(Integer id) {
        String query = "SELECT * FROM twats where id = ?";
        return jdbcTemplate.queryForObject(query, new Object[]{id}, new TwatMapper());
    }

    @Override
    public List<Twat> listTwats() {
        String query = "SELECT * FROM twats ORDER BY id DESC";
        return jdbcTemplate.query(query, new Object[]{}, new TwatMapper());
    }

    @Override
    public List<Twat> listTwats(Integer userid) {
        String query = "SELECT * FROM twats WHERE uid = ?  ORDER BY id DESC";
        return jdbcTemplate.query(query, new Object[]{userid}, new TwatMapper());
    }


    public void delete(Integer id, Integer userid) {
        Twat twat = getTwat(id);
        String deleteRetwats = "DELETE FROM twats WHERE content LIKE ?";
        String query = "DELETE FROM twats WHERE id = ? AND uid = ? LIMIT 1";
        //insert correct stuff in twat
        jdbcTemplate.update(query, id,userid);
        jdbcTemplate.update(deleteRetwats, "{retwat@" + twat.getId() + "}");
    }
}