package se.gurp2.models;

import org.springframework.jdbc.core.JdbcTemplate;
import se.gurp2.objects.JsonResponse;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by raz on 2015-05-25.
 */
public class LoveModel {
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplate;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
    private boolean userHasLoved(int userid, int twatid) {
        String query = "SELECT * FROM likes WHERE userid = ? AND twatid = ?";
        try {
            Connection conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(query);
            ps.setInt(1, userid);
            ps.setInt(2, twatid);
            ResultSet rs = ps.executeQuery();
            if (rs.first()) {
                conn.close();
                return true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return false;
    }
    public JsonResponse love(int userid, int twatid) {
        String create = "INSERT INTO likes (userid, twatid) VALUES (?, ?)";
        String delete = "DELETE FROM likes WHERE userid = ? AND twatid = ?";

        if(!this.userHasLoved(userid, twatid)) {
            jdbcTemplate.update(create, userid, twatid);
            return new JsonResponse(true, "loved");
        }else {
            jdbcTemplate.update(delete, userid, twatid);
            return new JsonResponse(true, "unloved");
        }
    }
}
