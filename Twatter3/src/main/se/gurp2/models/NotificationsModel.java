package se.gurp2.models;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import se.gurp2.objects.SearchResult;
import se.gurp2.objects.User;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by raz on 2015-05-28.
 */
public class NotificationsModel {
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplate;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public int getUnreadNotifications(int userid) {
        String query = "SELECT COUNT(*) AS amt FROM notifications WHERE userid = ? AND viewed = 0";
        Connection conn = null;
        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(query);
            ps.setInt(1, userid);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                int amt = rs.getInt("amt");
                conn.close();
                return amt;
            }
            conn.close();
            return 0;
        } catch (SQLException e) {
            e.printStackTrace();
            if(conn != null) {
                try {
                    conn.close();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
            return 0;
        }
    }

    public void markAllAsRead(int userid) {
        String query = "UPDATE notifications SET viewed = 1 WHERE userid = ?";
        jdbcTemplate.update(query, userid);
    }

    public List<SearchResult> getNotifications(int userid) {
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        TwatModel twatModel = (TwatModel) context.getBean("twat");
        ArrayList<SearchResult> results = new ArrayList<SearchResult>();
        String query = "SELECT * FROM notifications WHERE userid = ? ORDER BY id DESC LIMIT 25";
        Connection conn = null;
        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(query);
            ps.setInt(1, userid);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                results.add(new SearchResult(twatModel.getTwat(rs.getInt("twatid"))));
            }
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if(conn != null) {
                try {
                    conn.close();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        }
        return results;
    }

    public void create(String username, int twatid) {
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        UserModel userModel = (UserModel) context.getBean("user");
        try {
            User user = userModel.getUserByName(username);
            String query = "INSERT INTO notifications (twatid, userid) VALUES (?, ?)";
            jdbcTemplate.update(query, twatid, user.getId());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
