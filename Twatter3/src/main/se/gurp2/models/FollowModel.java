package se.gurp2.models;

import org.springframework.jdbc.core.JdbcTemplate;
import se.gurp2.objects.JsonResponse;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class FollowModel {
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplate;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
    public boolean userHasFollowed(int followerid, int targetid) {
        String query = "SELECT * FROM followers WHERE follower = ? AND target = ?";
        try {
            Connection conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(query);
            ps.setInt(1, followerid);
            ps.setInt(2, targetid);
            ResultSet rs = ps.executeQuery();
            if (rs.first()) {
                conn.close();
                return true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return false;
    }
    public JsonResponse follow(int followerid, int targetid) {
        String follow = "INSERT INTO followers (follower, target) VALUES (?, ?)";
        String unfollow = "DELETE FROM followers WHERE follower = ? AND target = ? LIMIT 1";


        if(!this.userHasFollowed(followerid, targetid)) {
            jdbcTemplate.update(follow, followerid, targetid);
            return new JsonResponse(true, "followed");
        }else {
            jdbcTemplate.update(unfollow, followerid, targetid);
            return new JsonResponse(false, "unfollowed");
        }
    }
    public List<Integer> getFollowing(int userid) {
        List<Integer> followed = new ArrayList<>();
        String query = "SELECT * FROM followers WHERE follower = ?";
        try {
            Connection conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(query);
            ps.setInt(1, userid);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                followed.add(rs.getInt("target"));
            }
            conn.close();
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return followed;
    }
    public List<Integer> getFollowers(int userid) {
        List<Integer> followed = new ArrayList<>();
        String query = "SELECT * FROM followers WHERE target = ?";
        try {
            Connection conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(query);
            ps.setInt(1, userid);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                followed.add(rs.getInt("follower"));
            }
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return followed;
    }
}
