package se.gurp2.models;

import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class StatsModel{

    private DataSource dataSource;
    private JdbcTemplate jdbcTemplate;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
    public int countUserTwats(int id) {
        String query = "SELECT COUNT(*) AS amt FROM twats WHERE uid = ?";
        try {
            Connection conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(query);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if(rs.next()) {
                return rs.getInt("amt");
            }
            conn.close();
            return 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }

    }

    public int countFollowers(int id) {
        String query = "SELECT COUNT(*) AS amt FROM followers WHERE target = ?";
        try {
            Connection conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(query);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if(rs.next()) {
                return rs.getInt("amt");
            }
            conn.close();
            return 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }

    }

    public int countFollowing(int id) {
        String query = "SELECT COUNT(*) AS amt FROM followers WHERE follower = ?";
        try {
            Connection conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(query);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if(rs.next()) {
                return rs.getInt("amt");
            }
            conn.close();
            return 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }

    }
}
