package se.gurp2.models;

import org.springframework.jdbc.core.JdbcTemplate;
import se.gurp2.interfaces.UserInterface;
import se.gurp2.mappers.UserMapper;
import se.gurp2.objects.User;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class UserModel implements UserInterface {
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplate;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public void create(String firstname, String lastname, String username, String password, String email) {

        String createUser = "INSERT INTO users (firstname,lastname,username, password, email) VALUES(?, ?, ?, ?, ?)";
        jdbcTemplate.update(createUser, firstname, lastname, username, password, email);

    }

    @Override
    public int verifyCredentials(String username, String password) {
        String query = "SELECT * FROM users WHERE username = ? AND password = ?";
        User user = jdbcTemplate.queryForObject(query, new Object[]{username, password}, new UserMapper());
        return (int) user.getId();
    }

    @Override
    public boolean logout(String token) {
        if(readToken(token) > 0){
            String query = "DELETE FROM tokens WHERE token = ? LIMIT 1";
            jdbcTemplate.update(query, token);
            return true;
        }
        return false;
    }

    @Override
    public User getUser(Integer id) {
        String query = "SELECT * FROM users WHERE id = ?";
        return jdbcTemplate.queryForObject(query, new Object[]{id}, new UserMapper());
    }
    public User getUserByName(String username) {
        String query = "SELECT * FROM users WHERE username = ? LIMIT 1";
        return jdbcTemplate.queryForObject(query, new Object[]{username}, new UserMapper());
    }
    public User getUserByToken(String token) {
        int id = readToken(token);
        return getUser(id);
    }

    @Override
    public boolean delete(Integer id) {
        return false;
    }

    public UUID createSession(int userid) {
        String deleteOldSession = "DELETE FROM tokens WHERE userid = ?";
        jdbcTemplate.update(deleteOldSession, userid);
        UUID token = UUID.randomUUID();
        String createNewSession = "INSERT INTO tokens (userid, token) VALUES(?, ?)";
        jdbcTemplate.update(createNewSession, userid, token.toString());
        return token;
    }
    public int readToken(String token) {
        String getUserId = "SELECT userid FROM tokens WHERE token = ?";
        Connection conn = null;
        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(getUserId);
            ps.setString(1, token);
            ResultSet rs = ps.executeQuery();
            if (rs.first()) {
                int userid = rs.getInt("userid");
                conn.close();
                return userid;
            }
            conn.close();
            return 0;
        } catch (SQLException e) {
            e.printStackTrace();
            if(conn != null) {
                try {
                    conn.close();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
            return 0;
        }
    }
    public boolean userExists(String username, String email) {
        String query = "SELECT * FROM users WHERE username = ? OR email = ?";
        try {
            Connection conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(query);
            ps.setString(1, username);
            ps.setString(2, email);
            ResultSet rs = ps.executeQuery();

            if (rs.first()) {
                conn.close();
                return true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return false;

    }



}