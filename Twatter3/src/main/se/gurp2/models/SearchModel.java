package se.gurp2.models;

import org.springframework.jdbc.core.JdbcTemplate;
import se.gurp2.mappers.TwatMapper;
import se.gurp2.mappers.UserMapper;
import se.gurp2.objects.SearchResult;
import se.gurp2.objects.Twat;
import se.gurp2.objects.User;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

public class SearchModel {
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplate;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
    public ArrayList<SearchResult> search(String query) {
        String userQuery = query.replace("@", "");
        ArrayList<SearchResult> results = new ArrayList<SearchResult>();
        String findUsers = "SELECT * FROM users WHERE firstname LIKE ? OR lastname LIKE ? OR username LIKE ?";
        List<User> users = jdbcTemplate.query(findUsers, new Object[]{"%" + userQuery + "%", "%" + userQuery + "%", "%" + userQuery + "%"}, new UserMapper());
        String findTwats = "SELECT * FROM twats WHERE content LIKE ?";
        List<Twat> twats = jdbcTemplate.query(findTwats, new Object[]{"%" + query + "%"}, new TwatMapper());
        for(User user : users) {
            results.add(new SearchResult(user));
        }
        for(Twat twat : twats) {
            results.add(new SearchResult(twat));
        }
        return results;
    }
}
